/*
 *  打印预览或另存为PDF功能，需要依赖:
 * ../plugin/printThis.js
 */
(function ( global, factory ) {

	'use strict';

	// export as AMD...
	if ( typeof define !== 'undefined' && define.amd ) {
		define( [   ], factory );
	}

	// ...or as browserify
	else if ( typeof module !== 'undefined' && module.exports ) {
		module.exports = factory(  );
	}
	else
		global.GooFlow = factory(  );

}( typeof window !== 'undefined' ? self : this, function (  ) {
	if(GooFlow.prototype.print && typeof GooFlow.prototype.print==='function'){
		return GooFlow;
	}
	//console.log('GooFlow.prototype.print',GooFlow);
	//扩展定义打印预览或另存为PDF功能的方法
	GooFlow.prototype.print=function(scale){
		var max=this._suitSize();
		if(!scale)  scale=1.0;
		max.width+=20;max.height+=20;
		var printDiv=this.$workArea.clone();
		printDiv.css({
			width:max.width+"px",height:max.height+"px"
		});
		printDiv.children(".GooFlow_work_group").css({width:max.width+"px",height:max.height+"px"});
		printDiv.children("svg").css({width:max.width+"px",height:max.height+"px"});
		printDiv.children(".GooFlow_work_vml").css({width:max.width+"px",height:max.height+"px"});
		printDiv=printDiv.wrap('<div class=\"GooFlow GooFlow_work\"></div>').parent();
		if(GooFlow.color.font){
			printDiv.css("color",GooFlow.color.font);
		}
		printDiv.css({"transform-origin": "top right", transform:"scale("+scale+")"}).printThis({
			debug:false,
			base:document.URL
		});
	}
	return GooFlow;
}));
