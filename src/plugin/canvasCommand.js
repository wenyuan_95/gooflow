var CanvasCommand = (function() {
	var Action = {
		//构建背景
		_initBg : function(width,height,dom){
			var canvas = document.createElement('canvas');
			canvas.width = width;
			canvas.height = height;
			var ctx = canvas.getContext('2d');
			ctx.fillStyle = '#ffffff';
			ctx.fillRect(0,0,canvas.width,canvas.height);
			return {
				canvas: canvas,
				fontFamily: dom.css('font-family'),
				fontSize: dom.css('font-size'),
				lineHeight: dom.css('line-height'),
			}
		},
		_toNum: function(str){
			return str!==null&&str!==''? parseInt(str.split('px')[0],10):undefined;
		},
		_analyseLabel: function(labelDom){
			var property = {
				fontSize:labelDom.css('font-size'),//文字大小
				text: labelDom.text(),//文字内容
				color: labelDom.css('color'),//文字颜色
				offsetWidth: labelDom[0].offsetWidth,//文字容器宽度
				offsetHeight: labelDom[0].offsetHeight,//文字容器高度
				offsetLeft:labelDom[0].offsetLeft+2,
				offsetTop:labelDom[0].offsetTop+2,
				lineNum: Math.ceil(labelDom[0].offsetHeight/20),//文字要分几行？
			};
			return property;
		},
		_analyseIcon: function(iconDom){
			var property={
				// top: this._toNum(iconDom[0].style.top),//
				// left: this._toNum(iconDom[0].style.left),//
				offsetLeft:iconDom[0].offsetLeft,
				offsetTop:iconDom[0].offsetTop,
				width: iconDom.width(),
				height: iconDom.height(),
				fontFamily: iconDom.css('font-family').split(' ')[0],//图标矢量字体库
				fontSize: iconDom.css('font-size'),//图标矢量字体大小
				lineHeight : this._toNum(iconDom.css('font-size')),//图标矢量字体行高
				backgroundImage: iconDom[0].style.backgroundImage,//图标背景图
				backgroundPositionX : iconDom[0].style.backgroundPositionX,//图标背景定位X
				backgroundPositionY : iconDom[0].style.backgroundPositionY,//图标背景定位Y
				color: iconDom.css('color'),//图标矢量字体颜色
				opacity: parseFloat(iconDom.css("opacity")),//图标矢量透明度
				content:window.getComputedStyle(iconDom[0],'::before').getPropertyValue('content')
			};
			return property;
		},
		_analyseArea : function(areaDom){
			var bg=areaDom.children(".bg");

			var property={
				top: this._toNum(areaDom[0].style.top),
				left: this._toNum(areaDom[0].style.left),
				width: areaDom.outerWidth(),
				height: areaDom.outerHeight(),
				borderColor: bg.css('border-top-color'),//区域边框颜色
				bgColor: bg.css('background-color'),//区域块背景色
				opacity: parseFloat(bg.css("opacity")),//区域块透明度
				icon: this._analyseIcon(areaDom.children('i')),
				label: this._analyseLabel(areaDom.children("label"))
			};
			return property;
		},
		_analyseNode : function(nodeDom){
			var property={
				top: this._toNum(nodeDom[0].style.top),//
				left: this._toNum(nodeDom[0].style.left),//
				width: nodeDom.outerWidth(),//
				height: nodeDom.outerHeight(),//
				borderColor: nodeDom.css('border-top-color'),//节点边框颜色
				borderWidth: this._toNum(nodeDom.css('border-top-width')),//节点边框宽度
				bgColor: nodeDom.css('background-color'),//节点背景色
				borderRadius: this._toNum(nodeDom.css("border-top-left-radius")),//节点圆角半径
				boxShadow: nodeDom.css("box-shadow"),//节点阴影
				icon: this._analyseIcon(nodeDom.find('i')),
			};
			if(nodeDom.hasClass("item_round")){
				property.label = this._analyseLabel(nodeDom.children(".span"));
			}else{
				property.label = this._analyseLabel(nodeDom.children("table").find("td:eq(1)"));
			}
			return property;
		},
		_fillIcon:function(bg, bgLeft,bgTop,icon){
			var ctx = bg.canvas.getContext('2d');
			if(icon.content){//矢量字体图标
				if(icon.content.indexOf('"')===0){
					icon.content = icon.content.split('"')[1];
				}
				//ctx.moveTo(bgTop+icon.offsetLeft, bgTop+icon.offsetTop);
				icon.color = icon.color.replace('rgb', 'rgba').replace(')', ', ' + icon.opacity + ')');
				ctx.fillStyle = icon.color;
				//ctx.strokeStyle = icon.color;
				ctx.font = icon.fontSize+' '+icon.fontFamily;
				ctx.textAlign='center';
				ctx.textBaseline='middle';
				//  绘制内容
				ctx.fillText(icon.content, bgLeft+icon.offsetLeft+13, bgTop+icon.offsetTop+4+icon.lineHeight/2)
				//ctx.strokeText(icon.content, bgLeft, bgTop)
			}else{//css spite背景定位图标

			}
		},
		//根据区域组信息在背景上画一堆区域组泳道（传参areas为要绘制的区域组详细json信息）
		renderAreas:function(bg,areas){
			var ctx = bg.canvas.getContext('2d');
			for(var key in areas) {
				var area = areas[key];
				//填充半透明矩形
				area.bgColor = area.bgColor.replace('rgb', 'rgba').replace(')', ', ' + area.opacity + ')');
				area.borderColor = area.borderColor.replace('rgb', 'rgba').replace(')', ', ' + area.opacity + ')');
				ctx.fillStyle = area.bgColor;
				ctx.strokeStyle = area.borderColor;
				ctx.rect(area.left, area.top, area.width, area.height);
				ctx.fill();
				ctx.stroke();
				this._fillIcon(bg, area.left-3, area.top-3, area.icon);
			}
		},

		//根据节点信息在背景上画一组节点（传参nodes为要绘制的节点详细json信息）
		renderNodes:function(bg,nodes){
			var ctx = bg.canvas.getContext('2d');
			for(var key in nodes){
				var node = nodes[key];
				//渲染阴影
				var sd = node.boxShadow.split(") ");
				if(sd.length===1){
					var tmp = node.boxShadow.split("rgba");
					sd[0]='rgba'+tmp[1];
					sd[1]=tmp[0];
				}
				ctx.shadowColor = sd[0];
				sd=sd[1].split(" ");
				ctx.shadowOffsetX=this._toNum(sd[0])+(node.borderWidth>1? 1:0);
				ctx.shadowOffsetY=this._toNum(sd[1])+(node.borderWidth>1? 1:0);
				ctx.shadowBlur=this._toNum(sd[2]);
				//填充圆角矩形
				ctx.fillStyle=node.bgColor;
				ctx.roundRect( node.left+node.borderWidth/2, node.top+node.borderWidth/2,
					node.width-node.borderWidth, node.height-node.borderWidth, node.borderRadius).fill();
				//加边框
				ctx.shadowBlur=0;
				ctx.shadowColor='';
				ctx.shadowOffsetX=0;
				ctx.shadowOffsetY=0;
				ctx.strokeStyle = node.borderColor;
				ctx.lineWidth = (node.borderWidth===0? 0.1:node.borderWidth);
				ctx.stroke();
				this._fillIcon(bg, node.left, node.top+(node.borderRadius>6? 1:0), node.icon);
			}
		},

		fillText: function(font) {
			var canvas2 = document.createElement('canvas');
			var sizes = [], width=0;
			$.each(font, function(key, value) {
				sizes.push(value['size']);
				width += value['size'] * value['txt'].length + 5;
			});
			canvas2.width = width - 5;//画布宽度
			var max = Math.max.apply(this, sizes);

			canvas2.height = max * 1.5;//画布高度
			var ctx2 = canvas2.getContext('2d');
			ctx2.fillStyle = "#ffed03";
			ctx2.fillRect(0, 0, canvas2.width, canvas2.height);
			var x = 0;
			$.each(font, function(key, value) {
				ctx2.font = (value['bold'] || '')+" "+value['size']+"px serif";
				ctx2.fillStyle = "black";
				ctx2.fillText(value['txt'], x, max);
				x += value['size'] * value['txt'].length + 5;
			});

			return canvas2;
		},
		fillImage: function(num, txts) {
			var qrcode = new Image();
			qrcode.src = 'img/qrcode.png';
			qrcode.onload = function() {
				var image = new Image();
				image.src = 'img/story/'+num+'.png';
				image.onload = function() {
					ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
					ctx.drawImage(qrcode, 20, 400, 80, 80);
					$.each(txts, function(key, value) {
						ctx.drawImage(value, value.left, value.top, value.width, value.height);
					});
					var base64 = canvas.toDataURL("image/jpeg", 0.6);
					$compose.attr('src', base64);
				};
			};
		}
	};
	return Action;
})();