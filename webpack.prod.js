/* 生产环境编译发布配置，直接编译 */
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.config.js');
const os = require('os');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common,{
	devtool: 'source-map',//生产环境用
	// plugins: [
	// 	new UglifyJSPlugin({
	// 		uglifyOptions: {
	// 			ie8: true,
	// 			mangle: true,
	// 			output: { comments: false },
	// 			compress: { warnings: false }
	// 		},
	// 		exclude:/\.min\.js$/,
	// 		sourceMap: true,
	// 		cache: true,
	// 		parallel: os.cpus().length * 2
	// 	}),//代码压缩
	// ],
});