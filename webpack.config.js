/* global require, module, __dirname */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	entry: {
		'GooFlow':'./src/GooFlow.js',
		'GooFlow.export':['./src/plugin/html2canvas.js','./src/plugin/canvg.js','./src/GooFlow.export.js'],
		'GooFlow.print':['./src/plugin/printThis.js','./src/GooFlow.print.js'],
		// 'GooFlow.print':['./src/GooFlow.print.js'],
	},
	plugins: [
		// new HtmlWebpackPlugin({
		// 	title: 'Output Management'
		// }),
		new webpack.NamedModulesPlugin(),//查看要修补(patch)的依赖
		new CleanWebpackPlugin(['dist']),//重编译前清空dist目录
	],
	output: {
		filename: '[name].js',
		chunkFilename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
		libraryTarget: 'umd',
		library: 'GooFlow'
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				include: path.resolve(__dirname, "src"),
				use: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				include: path.resolve(__dirname, "src/assets/img"),
				use: [ 'file-loader' ],
				// options: {
				// 	name: "img/[name].[ext]",
				// }
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				include: path.resolve(__dirname, "src/assets/fonts"),
				use: [ 'file-loader' ],
				// options: {
				// 	name: "fonts/[name].[ext]",
				// }
			}
		]
	}
};